﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SCHOOL_MANAGEMENT_SYSTEM.Models;

namespace SCHOOL_MANAGEMENT_SYSTEM.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public int BrandId { get; set; }
        public Branch Brand { get; set; }
        public string Sex { get; set; }
        public string FullName { get; set; }
        public bool IsDeleted { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Floor> Floors { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<CheckIn> CheckIns { get; set; }
        public DbSet<CheckInDetail> CheckInDetails { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<RoomDetail> RoomDetails { get; set; }



        public DbSet<HowtoUse> HowtoUses { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<Pricing> Pricings { get; set; }
        public DbSet<Condition> Conditions { get; set; }
        public DbSet<PayBy> PayBys { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Branch> Branchs{ get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Salary> Salarys { get; set; }
        public DbSet<Parent> Parents  { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<student> Students { get; set; }
        public DbSet<shifts> Shiftes { get; set; }
        public DbSet<grade> Grades { get; set; }
        public DbSet <emergency> Emergencys { get; set; }
        public DbSet<Parrentstudent> Parrentstudents { get; set; }
        public DbSet<appropriate> appropriates { get; set; }

        public DbSet<registerstudent> Registerstudents { get; set; }
        public DbSet<studylanguage> studylanguages { get; set; }
        public DbSet<studyperiod> studyperiods { get; set; }

        public DbSet<course> courses { get; set; }
        public DbSet <payment>Payments { get; set; }

        public DbSet<paymentdetail> PaymentDetails { get; set; }
        public DbSet <ExchangeRate>Exchanges { get; set; }

        //New
        public DbSet<Position> Position { get; set; }
        public DbSet<Showroom> Showroom { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet <Customer>Customer { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<InvoiceDetail> InvoiceDetail { get; set; }
        public DbSet<Bonus> Bonus { get; set; }
        public DbSet<ExpenseType> ExpenseTypes { get; set; }
        public DbSet<OtherExpense> OtherExpenses { get; set; }
        public DbSet<Transfer> Transfers { get; set; }
        public DbSet <invoice_move> InvoiceMoves { get; set; }
        public DbSet<Collectmoney> CollectMoneys { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<SCHOOL_MANAGEMENT_SYSTEM.Models.Staff> Staffs { get; set; }
    }
}